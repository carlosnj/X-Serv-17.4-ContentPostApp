import webapp


class contentAPP(webapp.Webapp):
    resource = {'/': 'home page', '/favicon.ico': 'There is no favicon for the moment.'}

    def parse(self, received):
        recibido = received.decode()
        protocol = recibido.split(" ")[0]
        client_resource = recibido.split(" ")[1] # o recibido.split('\r\n\r\n')[1]
        if protocol == "POST":
            POST_body = recibido.split("\n")[-1]
            if "content" in POST_body:
                POST_body = POST_body.split("=")[1]
        else:
            POST_body = None

        return protocol, client_resource, POST_body  # me quedo con la petición del cliente

    def process(self, analyzed):
        protocol, client_resource, POST_body = analyzed
        if protocol == 'POST':
            self.resource["/" + POST_body] = POST_body
        if client_resource in self.resource.keys():
            http = "200 OK"
            html = """
                    <!DOCTYPE html>
                    <html lang="en">
                      <body>
                        <p>{}</p>
                        <form action="/" method="POST">
                          Say something: <input name="content" type="text" />
                        <input type="submit" value="Submit" />
                        </form>
                      </body>
                    </html>
                    """
            html = html.format(self.resource[client_resource])
        else:
            http = "404 Not Found"
            html = f'<html><body><img src="https://upload.wikimedia.org/wikipedia/commons/9/9b/404-error-css.png"/>' \
                   + '</body></html>'
        print(self.resource)
        return http, html


try:
    if __name__ == "__main__":
        content = contentAPP('localhost', 1234)
except KeyboardInterrupt:
    print("Server has been closed.")
